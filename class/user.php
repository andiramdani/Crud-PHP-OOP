<?php
	include_once "koneksi.php";

	class user
		{
			// menampilkan data user 
			public function tampilUser()
				{
					$u = new koneksi();
					$sql = "SELECT * FROM user ORDER BY id DESC";
					$tampil = $u->getKoneksi()->query($sql);
					$row = $u->getKoneksi()->affected_rows;
					if(!empty($row))
						{
							while($data = $tampil->fetch_array(MYSQLI_BOTH))
								{
									$dataUser[] = $data;
								}
						}
					return $dataUser;
				}

			// menambahkan data user baru
			public function tambahUser(Array $data)
				{
					$tambah = new koneksi(); // membuat objek koneksi baru
					$data = implode(",",$data);
					$sql = "INSERT INTO user VALUES('',$data)";
					$input = $tambah->getKoneksi()->query($sql);
					if($input)
						{
							echo "<script>alert('Data user berhasil ditambahkan');location.replace('index.php')</script>";
						}
					else
						{
							echo "<script>alert('data user gagal disimpan !!!!!');location.replace('index.php')</script>";
						}
				}

			// mengedit data user 
			public function editUser($id)
				{
					$u = new koneksi();
					$sql = "SELECT * FROM user where id = '$id' ";
					$tampil = $u->getKoneksi()->query($sql);
					$row = $u->getKoneksi()->affected_rows;
					if(!empty($row))
						{
							while($data = $tampil->fetch_array(MYSQLI_BOTH))
								{
									$dataUser[] = $data;
								}
						}

					return $dataUser;
				}

			// update data user 
			public function updateUser($id)
				{

					$update = new koneksi();
					
					$email 		= $_POST['email'];
					$password 	= $_POST['password'];

					$sql = "UPDATE user SET email='$email', password='$password' WHERE id ='$id' ";
					$updatedata = $update->getKoneksi()->query($sql);
					$row = $update->getKoneksi()->affected_rows;
					
					if($updatedata)
						{
							echo "<script>alert('Data berhasil diperbarui');location.replace('index.php')</script>";
						}
					else
						{
							echo "<script>alert('Data gagal diperbarui')location.replace('edit_user.php?id=$id')</script>";
						}
				}

			// menghapus data user
			public function hapusUser($id)
				{
					$hapus = new koneksi();
					$sql = "DELETE FROM user WHERE id='$id'";
					$hps = $hapus->getKoneksi()->query($sql);
					$row = $hapus->getKoneksi()->affected_rows;
					if($hps)
						{
							echo "<script>alert('Data Berhasil Dihapus Boss');
					    location.replace('index.php')</script>";
						}
					else
						{
							echo "<script>alert('Data Gagal Dihapus Bos');
					    location.replace('index.php')</script>";
						}
				}
		}